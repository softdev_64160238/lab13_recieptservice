/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author User
 */
public class TestCustomerService {

    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
        System.out.println(cs.getByTel("0000000001"));

        Customer cus1 = new Customer("X", "0000000002");
        cs.addNew(cus1);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }

        Customer delCus = cs.getByTel("0000000002");
        delCus.setTel("0000000003");
        cs.update(delCus);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }

        cs.delete(delCus);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
    }
}
